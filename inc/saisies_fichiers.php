<?php

/**
 *  
**/
function saisir_fichier($saisie, $desc, $options) {
	$type = substr($saisie['saisie'], 5);
	if ($saisir_fichier_type = charger_fonction('saisir_fichier_' . $type, 'inc', true)) {
		$res = $saisir_fichier_type($saisie, $desc, $options);
		if (isset($res['erreur']) and $res['erreur']) {
			return $res['erreur'];
		}
	} else {
		spip_log("! aucun traitement associé à la saisie de fichier $type");
	}
	return true;
}



function inc_saisir_fichier_pdf($saisie, $desc, $options){
	// on stocke dans /form/$id/$id_auteur_ + hash.pdf
	$dir = sf_creer_repertoires_stockage_fichier($options);
	$name = $saisie['options']['nom'];
	
	$erreur = '';
	
	$i = pathinfo($desc['name']);
	$i['extension'] = strtolower($i['extension']);
	
	// n'autoriser que les pdf
	if ($i['extension'] == 'pdf') {
		sf_copier_fichier_et_stocker_nom($name, "pdf", $dir, $desc, $options);
	} else {
		$erreur = _T('saisie_fichier:erreur_format', array('format'=>'PDF'));
	}
	
	return array(
		'erreur' => $erreur,
	);
}



function inc_saisir_fichier_jpg($saisie, $desc, $options){
	// on stocke dans /form/$id/$id_auteur_ + hash.pdf
	$dir = sf_creer_repertoires_stockage_fichier($options);
	$name = $saisie['options']['nom'];
	
	$erreur = '';
	
	$i = pathinfo($desc['name']);
	$i['extension'] = strtolower($i['extension']);
	
	// n'autoriser que les pdf
	if (in_array($i['extension'], array('jpg','jpeg'))) {
		sf_copier_fichier_et_stocker_nom($name, "jpg", $dir, $desc, $options);
	} else {
		$erreur = _T('saisie_fichier:erreur_format', array('format'=>'JPG'));
	}
	
	return array(
		'erreur' => $erreur,
	);
}


/**
 * Crée les répertoires dans IMG/ pour stocker les fichiers reçus 
 *
 * @param array $options 	Options tel que id_formulaire
 * @return string 	Chemin du répertoire
**/
function sf_creer_repertoires_stockage_fichier($options) {
	sous_repertoire($dir = _DIR_IMG . 'form/');
	if (isset($options['id_formulaire'])) {
		sous_repertoire($dir .= $options['id_formulaire'] . "/");	
	}
	return $dir;
}

/**
 * Copie un fichier envoyé par un formulaire dans le répertoire désigné
 * et pose l'adresse du fichier depuis IMG dans $_GET pour que
 * les traitements le considèrent comme une valeur postée et l'enregistrent 
 *
 * @param 
 * @return 
**/
function sf_copier_fichier_et_stocker_nom($name, $extension, $dir, $desc, $options) {

	$nom = $options['id_auteur'] .  '_'  . substr(md5($name), 0, 6) . '.' . $extension;
	
	// deplacer au nouvel emplacement.
	include_spip('inc/getdocument');
	if (deplacer_fichier_upload($desc['tmp_name'], $dir . $nom, true)) {
		// donner a manger au traiter en forcant l'enregistrement
		// sur la localisation du fichier
		set_request($name, "form/" . $options['id_formulaire'] . "/" . $nom);
	}	
}

?>
