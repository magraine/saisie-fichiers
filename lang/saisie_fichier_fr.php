<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'saisie_file_jpg_titre' => 'Fichier Image (JPG)',
	'saisie_file_jpg_explication' => "Envoi d'un fichier image (extension jpg)",
	
	'saisie_file_pdf_titre' => 'Fichier PDF',
	'saisie_file_pdf_explication' => "Envoi d'un fichier PDF",
	
	'erreur_format' => "Le fichier envoyé doit être au format @format@.",
	
	'erreur_format_pdf' => "Le fichier envoyé doit être au format PDF.",
	'erreur_upload_fichier' => "Erreur lors du téléchargent du fichier...",
	
);

?>
