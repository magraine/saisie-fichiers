<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

///  Fichier produit par PlugOnet
// Module: paquet-saisie_fichiers
// Langue: fr
// Date: 11-09-2014 15:16:33
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// S
	'saisie_fichiers_description' => 'Ce plugin permet d\'envoyer des fichiers
		de type PDF ou JPG dans le formulaire formidable. 
		Condition : que le formulaire soit lié à une personne identifiée.
		
		Il exploite les plugins : Formidable et Saisies.',
	'saisie_fichiers_slogan' => 'Ce plugin permet d\'envoyer des fichiers',
);
?>